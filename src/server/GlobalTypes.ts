
export interface IService<T> 
{ 
    findByID(ID: string) : Promise<T>;
    findAll(): Promise<T[]>;
    getPage(pageNum : number, pageSize : number): Promise<T[]>
    save(item:T) : Promise<T>;
    deleteByID(ID: string) : Promise<T>;
    updateByID(ID: string,item: T) : Promise<T>;
}


export interface IUserService extends IService<User>{
    findByEmail(email: string) : Promise<User>;
}

export type PlaylistID = string;

export interface Playlist{
    id ?: PlaylistID;
    name ?: string;
    songs ?: SongID[];
}

export type SongID = string;
export interface Song{
    id ?: SongID
    name ?: string;
    length ?: number;
    artist ?: ArtistID;
    playlists ?: PlaylistID[];
}


export type ArtistID = string;
export interface Artist{
    id ?: ArtistID
    name ?: string;
    songs ?: SongID[];
}


export interface User {
    id ?: string,
    refresh_token ?: string,
    first_name ?: string,
    last_name ?: string,
    email ?: string,
    password ?: string,
    phone ?: string

}

    
    