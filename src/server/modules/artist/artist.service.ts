import ArtistMongoService from "./artist.service.Mongo.js";
import { IService,Artist } from "../../GlobalTypes.js";


const { DB_TYPE } = process.env;
let service : IService<Artist>;

const db_type_lower: string = (DB_TYPE as string).toLowerCase();
switch (db_type_lower) {
    case "mongo":
        service =  new ArtistMongoService();
        break;
    default:
        throw new Error("Service is not defined");
}

export default service;

   
