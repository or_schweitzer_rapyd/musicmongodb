/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import express from "express";
  import { Request,Response, NextFunction } from "express";
  import songService from "../song/song.service.js";
  import artistService from "./artist.service.js";
  import { Artist,ArtistID } from "../../GlobalTypes.js";
  import mongoose from "mongoose";
  
  import {
      validateUpdateUser,
      validateCreateUser,
  } from "../../middleware/validation.mw.js";
  
  const router = express.Router({mergeParams:true});
  
  // parse json req.body on post routes
  router.use(express.json());

 //search for given artist in db
 // if not found throw an error
async function findByIdElseThrow(ID : string){
    const foundArtist = await (artistService.findByID(ID));
    
        if(foundArtist !== null){
            return foundArtist;
        }else{
            throw new Error(ID);
        }
}

//======================Routing Functions========================  

//This router path is :  /artists/:artistID/songs
  


// CREATES A NEW SONG FOR A GIVEN ARTIST   
  router.post("/",
      //raw(validateCreatePlaylist,
      raw(async (req: Request, res:Response) => {

        //searching for artist with identical id
        const givenArtist = (await findByIdElseThrow(req.params.artistID)) as Artist;
        req.body.artist = givenArtist.id;
        
        //add song
        const song = await songService.save(req.body);
        
        res.status(200).json(song);
      })
  );

  
  // GET ALL SONGS OF A GIVEN ARTIST
  router.get(
      "/",
      raw(async (req:Request, res:Response) => {
        const allSongs = await (songService.findAll());
        const artistSongs = allSongs.filter(song => song.artist?.toString() === req.params.artistID);
        res.status(200).json({artistSongs});
        })
  );
    
 
  
  export default router;
  