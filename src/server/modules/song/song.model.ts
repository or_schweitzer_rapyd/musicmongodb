import mongoose from "mongoose";
const { Schema, model } = mongoose;

export const SongSchema = new Schema(
    {
        name: { type: String, required: true },
        length: { type:Number,required: true},
        playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }],
        artist: { type: Schema.Types.ObjectId, ref: "artist" }
    },
    { timestamps: true }
);

export default model("song", SongSchema);
