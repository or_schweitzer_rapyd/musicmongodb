import SongMongoService from "./song.service.Mongo.js";
import { IService,Song } from "../../GlobalTypes.js";


const { DB_TYPE } = process.env;
let service : IService<Song>;

const db_type_lower: string = (DB_TYPE as string).toLowerCase();
switch (db_type_lower) {
    case "mongo":
        service =  new SongMongoService();
        break;
    default:
        throw new Error("repository is not defined");
}

export default service;

   
