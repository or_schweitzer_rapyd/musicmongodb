/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import express from "express";
  import { Request,Response, NextFunction } from "express";
  import songService from "../song/song.service.js";
  import playlistService from "./playlist.service.js";
  import { Playlist, PlaylistID, Song, SongID } from "../../GlobalTypes.js";
  import mongoose from "mongoose";
  
  import {
      validateUpdateUser,
      validateCreateUser,
  } from "../../middleware/validation.mw.js";
  
  const router = express.Router({mergeParams:true});
  
  // parse json req.body on post routes
  router.use(express.json());

 //search for given artist in db
 // if not found throw an error
async function findByIdElseThrow(ID : string){
   
    const foundPlaylist = await (playlistService.findByID(ID));
    
        if(foundPlaylist !== null){
            return foundPlaylist;
        }else{
            console.log("throw here");
            throw new Error(ID);
        }
}

//======================Routing Functions========================  

//This router path is :  /playlists/:playlistID/songs
  


// Add SONG FOR A GIVEN PLAYLIST   
  router.post("/",
      //raw(validateCreatePlaylist,
      raw(async (req: Request, res:Response) => {

        //searching for playlist with identical id
        const givenPlaylist = (await findByIdElseThrow(req.params.playlistID)) as Playlist;
       
         //searching for song with identical song id
         const foundSong = (await songService.findByID(req.body.id)) as Song;
         if(foundSong === null){
             throw new Error("song not exist");
         }

        //update song
        foundSong.playlists?.push(req.params.playlistID);
        await songService.updateByID(foundSong.id as SongID,
                                     {"playlists" : foundSong.playlists}
                                     );

        //update playlist
        givenPlaylist.songs?.push(foundSong.id as SongID);
        const updatedPlaylist = await playlistService.updateByID(givenPlaylist.id as PlaylistID,
            {"songs" : givenPlaylist.songs});
        
        
        res.status(200).json(updatedPlaylist);
      })
  );


  // DELETE SONG FROM A GIVEN PLAYLIST 
  router.delete("/:songID",
      //raw(validateCreatePlaylist,
      raw(async (req: Request, res:Response) => {

        //searching for playlist with identical id
        const givenPlaylist = (await findByIdElseThrow(req.params.playlistID)) as Playlist;
       
         //searching for song with identical song id
         const foundSong = (await songService.findByID(req.params.songID)) as Song;
         if(foundSong === null){
             throw new Error("song not exist");
         }


         //delete given playlist from song's playlist
        foundSong.playlists = foundSong.playlists?.filter(
            (p:PlaylistID) =>  p.toString() !==  req.params.playlistID);

        //delete song from given playlist
        givenPlaylist.songs = givenPlaylist.songs?.filter(
            (s:SongID) =>  s.toString() !==  req.params.songID);


        //update song
        await songService.updateByID(foundSong.id as SongID,
                                     {"playlists" : foundSong.playlists}
                                     );


        //update playlist
        const updatedPlaylist = await playlistService.updateByID(givenPlaylist.id as PlaylistID,
            {"songs" : givenPlaylist.songs});
        
        
        res.status(200).json(updatedPlaylist);
      })
  );
  

  
  
  export default router;
  