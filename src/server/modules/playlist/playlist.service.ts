import PlaylistMongoService from "./playlist.service.Mongo.js";
import { IService,Playlist } from "../../GlobalTypes.js";


const { DB_TYPE } = process.env;
let service : IService<Playlist>;

const db_type_lower: string = (DB_TYPE as string).toLowerCase();
switch (db_type_lower) {
    case "mongo":
        service =  new PlaylistMongoService();
        break;
    default:
        throw new Error("repository is not defined");
}

export default service;

   
