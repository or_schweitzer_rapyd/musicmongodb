import mongoose from "mongoose";
const { Schema, model } = mongoose;

export const PlaylistSchema = new Schema(
    {
        name: { type: String, required: true },
        songs: [{ type: Schema.Types.ObjectId, ref: "song" }],
        user: { type: Schema.Types.ObjectId, ref: "user" }
    },
    { timestamps: true }
);

export default model("playlist", PlaylistSchema);
