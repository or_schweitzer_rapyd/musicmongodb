import HttpException from "./Http.exception.js";

 
class ArtistNotFoundException extends HttpException {
  constructor(artistID: string) {
    super(400, `artist ${artistID} not exist ...`);
  }
}
 
export default ArtistNotFoundException;