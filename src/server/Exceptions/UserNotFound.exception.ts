import HttpException from "./Http.exception.js";

 
class UserNotFoundException extends HttpException {
  constructor(userID: string) {
    super(400, `user ${userID} not exist ...`);
  }
}
 
export default UserNotFoundException;