import HttpException from "./Http.exception.js";

 
class SongNotFoundException extends HttpException {
  constructor(songID: string) {
    super(400, `song ${songID} not exist ...`);
  }
}
 
export default SongNotFoundException;