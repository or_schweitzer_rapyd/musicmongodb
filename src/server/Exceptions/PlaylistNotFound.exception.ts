import HttpException from "./Http.exception.js";

 
class PlaylistNotFoundException extends HttpException {
  constructor(playlistID: string) {
    super(400, `playlist ${playlistID} not exist ...`);
  }
}
 
export default PlaylistNotFoundException;
