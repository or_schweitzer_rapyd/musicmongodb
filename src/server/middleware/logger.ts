import fs from "fs/promises";
import { Request, Response, NextFunction } from "express";

export const logger = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const date = new Date();
        const dateStr = `${date.toDateString()} ${date.toTimeString()}`;
        await fs.writeFile(
            `${process.cwd()}/mylog.log`,
            ` ID : ${req.myID} ${req.method}, ${req.originalUrl} on ${dateStr}\n`,
            {
                flag: "a",
            }
        );
        next();
    } catch (err) {
        next(err);
    }
};
