


import raw from "./route.async.wrapper.js";
import {createUserschema,updateUserschema} from "../modules/user/user.validation.js";
import { Request,Response, NextFunction } from "express";

export async function validateCreateUser(req:Request,res:Response,next:NextFunction){
  const result =  createUserschema.validate(req.body);
  console.log(result);
  if(result.error){
    throw new Error(result.error.details[0].message);
  }

  next();
}

export async function validateUpdateUser(req:Request,res:Response,next:NextFunction){
  const result =  updateUserschema.validate(req.body);
  console.log(result);
  if(result.error){
    throw new Error(result.error.details[0].message);
  }
  next();

}

