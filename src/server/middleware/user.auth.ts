import jwt from "jsonwebtoken";
import { User } from "../GlobalTypes.js";
import { Request, Response, NextFunction } from "express";
import userService from "../modules/user/user.service.js";
import bcrypt from "bcrypt";
import cookieParser from "cookie-parser";
import ms from "ms";

const {
    APP_SECRET = "123",
    TOKEN_EXPIRATION = "123",
    REFRESH_EXPIRATION = "123",
} = process.env;
//---------------------------------------------------------
export const generateToken = (user: User, secret: string, expiration: string) =>
    jwt.sign({ id: user.id }, secret, {
        expiresIn: REFRESH_EXPIRATION, 
    });
//---------------------------------------------------------
export const genRefreshToken = (user: User) =>
                                      generateToken(user,APP_SECRET,REFRESH_EXPIRATION);
//---------------------------------------------------------
export const genAccessToken = (user: User) =>
                                      generateToken(user,APP_SECRET,TOKEN_EXPIRATION);
//---------------------------------------------------------
const setAccessCookie = (res: Response , accessToken :any) =>{
    res.cookie("access_token", accessToken, {
        maxAge: ms(`${TOKEN_EXPIRATION}`),
        httpOnly: true,
    });

};
//---------------------------------------------------------

const setRefreshCookie = (res: Response,refreshToken : any) =>{
    res.cookie("refresh_token", refreshToken, {
        maxAge: ms(`${REFRESH_EXPIRATION}`),
        httpOnly: true,
    });

};

//---------------------------------------------------------
export const logIn = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const { password, email } = req.body;
    const testUser = await userService.findByEmail(email);
    const samePassword = await bcrypt.compare(
        password,
        testUser.password as string
    );

    if (email === testUser.email && samePassword) {
        // create tokens
        const refreshToken = genRefreshToken(testUser);
        const accessToken = genAccessToken(testUser);

        //set cookies
        setAccessCookie(res,accessToken);
        setRefreshCookie(res,refreshToken);
        
        //update userInDb
        
        await userService.updateByID(testUser.id as string,{refresh_token : refreshToken });
        //send response
        res.status(200).json({
            status: "Authorized",
            payload: `you are authenticated as ${email}`,
        });
    } else {
        res.status(403).json({
            status: "Unauthorized",
            payload: "wrong email or password",
        });
    }
};


//---------------------------------------------------------
export const checkToken = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    console.log("check token");
    const { refresh_token, access_token } = req.cookies;
    
    try {
         
        // verifies access token
        const decAccessTok = await jwt.verify(access_token, APP_SECRET);
        req.userID = decAccessTok.id;
        console.log(req.userID);
        console.log("access is valid");
        
        next();

    } catch (err) {
       
        //access token is not valid
        try{
            //try to generate a new access token with the refresh one
            console.log("access token is not valid");
            const decRefreshTok = await jwt.verify(refresh_token, APP_SECRET); 
            req.userID = decRefreshTok.id;
            console.log(`user is ${req.userID}`);

            //check user refresh token in DB
            const userInDb = await userService.findByID(req.userID);
            if(userInDb.refresh_token  === refresh_token){
                //if refresh token is valid then generate new access token
                const newAccessTok = genAccessToken(userInDb);
                setAccessCookie(res,newAccessTok);
                setRefreshCookie(res,refresh_token);
               
                next();
            }
            
        }catch(e){
            //please log in
            res.status(403).json({
                status: "Unauthorized",
                payload: "refresh token is not valid. please login again.",
            });
          
        }
        
            
    }
};

//---------------------------------------------------------
export const logOut = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try{
        const {refresh_token} = req.cookies;
        const decRefreshTok = await jwt.verify(refresh_token, APP_SECRET); 
        req.userID = decRefreshTok.id;
        const userInDb = await userService.findByID(req.userID);
        if(userInDb.refresh_token !== refresh_token) {
            throw Error("logout failed");
        }
        const user =  await userService.updateByID(req.userID,{refresh_token : ""});
        res.status(200).json({
            status:200,
            payload : `bye bye ${user.email} `
        });
    }catch(e){
        res.status(400).json({status: 400,
            payload:"logout failed"
        });
    }
    
    
};