import express from "express";
import { logIn,logOut } from "./middleware/user.auth.js";
import raw from "./middleware/route.async.wrapper.js";
const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

router.post("/logIn",raw(logIn));

router.post("/logOut",raw(logOut));



export default router;